package com.example.km.genericapp.fragments;

import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.km.genericapp.R;
import com.example.km.genericapp.constants.Constants;
import com.example.km.genericapp.models.posts.BlogItem;
import com.example.km.genericapp.network.PostalApiService;
import com.example.km.genericapp.utilities.ImageHelper;
import com.example.km.genericapp.utilities.SnackbarHelper;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Displays the full post details including the title, body, username, and number of comments.
 */
public class PostDetailsFragment extends BaseFragment {

    private CompositeDisposable disposables;
    private int postId;
    private ConstraintLayout constraintLayout;
    private TextView titleTextView;
    private TextView bodyTextView;
    private TextView userNameTextView;
    private TextView numberOfCommentsTextView;
    private ImageView avatarImageView;

    public void setPostId(int postId) {
        this.postId = postId;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        View view = inflater.inflate(R.layout.fragment_post_details, container, false);
        setupScreenLayout(view);
        showProgressIndicator();
        disposables = new CompositeDisposable();
        return view;
    }

    private void loadApiData() {
        disposables.add(PostalApiService.getBlogItem(postId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<BlogItem>() {
                    @Override
                    public void onComplete() {
                        hideProgressIndicator();
                        SnackbarHelper.showSnackbar(getActivity(), constraintLayout, getActivity().getString(R.string.api_call_complete));
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        handleError(throwable);
                    }

                    @Override
                    public void onNext(BlogItem blogItem) {
                        handleResponse(blogItem);
                    }
                }));
    }

    private void handleResponse(BlogItem blogItem) {
        titleTextView.setText(blogItem.getPost().getTitle());
        String body = blogItem.getPost().getBody();
        body = body.replace(Constants.NEWLINE, Constants.SPACE);
        bodyTextView.setText(body);
        userNameTextView.setText(blogItem.getUser().getUsername());
        numberOfCommentsTextView.setText(String.valueOf(blogItem.getComments().size()));
        ImageHelper.loadAvatar(avatarImageView, blogItem.getUser().getEmail());
    }

    private void handleError(Throwable error) {
        SnackbarHelper.showSnackbar(getActivity(), constraintLayout, getActivity().getString(R.string.api_call_error) + error.getLocalizedMessage());
    }

    @Override
    public void onResume() {
        super.onResume();
        loadApiData();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        disposables.clear();
    }

    @Override
    protected void setupScreenLayout(View view) {
        super.setupScreenLayout(view);
        constraintLayout = (ConstraintLayout) view.findViewById(R.id.constraintLayout);
        titleTextView = (TextView) view.findViewById(R.id.title);
        bodyTextView = (TextView) view.findViewById(R.id.body);
        userNameTextView = (TextView) view.findViewById(R.id.userName);
        numberOfCommentsTextView = (TextView) view.findViewById(R.id.numberOfComments);
        avatarImageView = (ImageView) view.findViewById(R.id.avatar);
    }
}
