package com.example.km.genericapp.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.km.genericapp.R;

import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;
import io.reactivex.subjects.Subject;

public class SettingsFragment extends BaseFragment {

    private Subject<Boolean> postsSubject;
    private Subject<Boolean> recipesSubject;
    private Subject<Boolean> postsWithDetailsSubject;
    private Subject<Boolean> versionsSubject;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_settings, container, false);
        Button postsButton = (Button) rootView.findViewById(R.id.btnPosts);
        postsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                postsSubject.onNext(true);
            }
        });
        Button settingsButton = (Button) rootView.findViewById(R.id.btnRecipes);
        settingsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                recipesSubject.onNext(true);
            }
        });
        // Java 7 syntax with anonymous class implementation.
//        Button postsWithDetailsButton = (Button) rootView.findViewById(R.id.btnPostsWithDetails);
//        postsWithDetailsButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                postsWithDetailsSubject.onNext(true);
//            }
//        });
//        Button versionsButton = (Button) rootView.findViewById(R.id.btnVersions);
//        versionsButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                versionsSubject.onNext(true);
//            }
//        });
        // Java 8 syntax with lambda expressions.
        Button postsWithDetailsButton = (Button) rootView.findViewById(R.id.btnPostsWithDetails);
        postsWithDetailsButton.setOnClickListener(view -> postsWithDetailsSubject.onNext(true));
        Button versionsButton = (Button) rootView.findViewById(R.id.btnVersions);
        versionsButton.setOnClickListener(view -> versionsSubject.onNext(true));
        return rootView;
    }

    public Observable<Boolean> launchPosts() {
        if (postsSubject == null) {
            postsSubject = PublishSubject.create();
        }
        return postsSubject;
    }

    public Observable<Boolean> launchRecipes() {
        if (recipesSubject == null) {
            recipesSubject = PublishSubject.create();
        }
        return recipesSubject;
    }

    public Observable<Boolean> launchPostsWithDetails() {
        if (postsWithDetailsSubject == null) {
            postsWithDetailsSubject = PublishSubject.create();
        }
        return postsWithDetailsSubject;
    }

    public Observable<Boolean> launchVersions() {
        if (versionsSubject == null) {
            versionsSubject = PublishSubject.create();
        }
        return versionsSubject;
    }
}
