package com.example.km.genericapp.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.km.genericapp.R;
import com.example.km.genericapp.interfaces.RecyclerViewClickListener;
import com.example.km.genericapp.models.posts.BlogItem;
import com.example.km.genericapp.viewholders.PostListCardViewHolder;

import java.util.ArrayList;

/**
 * Post list card adapter.
 */
public class PostListCardAdapter extends RecyclerView.Adapter<PostListCardViewHolder> {

    private RecyclerViewClickListener listener;

    private final ArrayList<BlogItem> blogItems;

    public PostListCardAdapter(ArrayList<BlogItem> blogItems, RecyclerViewClickListener listener) {
        this.blogItems = blogItems;
        this.listener = listener;
    }

    @Override
    public PostListCardViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        final View view = inflater.inflate(R.layout.post_list_card_item, parent, false);
        return new PostListCardViewHolder(view, listener);
    }

    @Override
    public void onBindViewHolder(PostListCardViewHolder holder, int position) {
        final BlogItem blogItem = blogItems.get(position);
        holder.setData(blogItem, position);
    }

    @Override
    public int getItemCount() {
        if (blogItems == null) {
            return 0;
        } else {
            return blogItems.size();
        }
    }
}
