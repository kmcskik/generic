package com.example.km.genericapp.utilities;

import android.widget.ImageView;

import com.example.km.genericapp.constants.Constants;
import com.example.km.genericapp.constants.Urls;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestCreator;

/**
 * Image helper.
 */
public final class ImageHelper {

    public static void loadAvatar(ImageView imageView, String emailAddress) {
        emailAddress = emailAddress.trim();
        emailAddress = emailAddress.toLowerCase();
        String url = Urls.ADORABLE_AVATARS_BASE_URL + emailAddress + Constants.PNG;
        loadImageByUrl(imageView, url);
    }

    public static void loadImageByUrl(ImageView view, String url) {
        RequestCreator creator = Picasso.with(view.getContext()).load(url);
        creator.into(view);
    }
}
