package com.example.km.genericapp.models.posts;

import java.util.ArrayList;

/**
 * Blog item containing all data related to a post.
 */
public class BlogItem {

    private Integer id;
    private Post post;
    private User user;
    private ArrayList<Comment> comments;

    public BlogItem(int postId) {
        this.id = postId;
        this.post = new Post();
        this.user = new User();
        this.comments = new ArrayList<>();
    }

    public BlogItem(Post post, User user, ArrayList<Comment> comments) {
        this.id = post.getId();
        this.post = post;
        this.user = user;
        this.comments = comments;
    }

    public Integer getId() {
        return id;
    }

    public Post getPost() {
        return post;
    }

    public void setPost(Post post) {
        this.post = post;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public ArrayList<Comment> getComments() {
        return comments;
    }

    public void setComments(ArrayList<Comment> comments) {
        this.comments = comments;
    }
}
