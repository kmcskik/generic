package com.example.km.genericapp.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.example.km.genericapp.R;
import com.example.km.genericapp.fragments.PostDetailsFragment;
import com.example.km.genericapp.models.posts.Post;
import com.example.km.genericapp.utilities.SnackbarHelper;

/**
 * Post details activity.
 */
public class PostDetailsActivity extends BaseActivity {

    private static final String POST_ID = "POST_ID";
    private int postId;

    public static void start(Activity activity, Post post) {
        Intent intent = new Intent(activity, PostDetailsActivity.class);
        Bundle bundle = new Bundle();
        bundle.putInt(POST_ID, post.getId());
        intent.putExtras(bundle);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_details);
        setupActionBar();
        setupLayout();
        loadPostDetailsFragment();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putInt(POST_ID, postId);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        postId = savedInstanceState.getInt(POST_ID);
    }

    @Override
    protected void setupActionBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setHomeButtonEnabled(true);
            actionBar.setTitle(getString(R.string.title_activity_post_details));
            actionBar.setDisplayShowTitleEnabled(true);
            actionBar.setHomeAsUpIndicator(R.drawable.ic_arrow_back);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    private void setupLayout() {
        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SnackbarHelper.showSnackbar(PostDetailsActivity.this, view, getString(R.string.fab_click_message));
            }
        });
    }

    private void loadPostDetailsFragment() {
        PostDetailsFragment fragment = new PostDetailsFragment();
        loadFragment(fragment);
        if (fragment != null) {
            Bundle bundle = getIntent().getExtras();
            postId = bundle.getInt(POST_ID);
            fragment.setPostId(postId);
        }
    }
}
