package com.example.km.genericapp.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.km.genericapp.R;

public class MainFragment extends BaseRecyclerFragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        setupScreenLayout(view);
        initializeRecyclerView();
        return view;
    }
}
