package com.example.km.genericapp.network;

import com.example.km.genericapp.constants.Urls;
import com.example.km.genericapp.models.versions.AndroidVersion;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;

/**
 * JSON placeholder API service.
 */
public class AndroidVersionApiService {
    private static AndroidVersionApiInterface retrofitService;

    private static AndroidVersionApiInterface getService() {
        if (retrofitService == null) {
            retrofitService = new Retrofit.Builder()
                    .baseUrl(Urls.ANDROID_VERSIONS_API_BASE_URL)
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build().create(AndroidVersionApiInterface.class);
        }
        return retrofitService;
    }

    public static Observable<List<AndroidVersion>> getAndroidVersions() {
        return getService().register();
    }

    public interface AndroidVersionApiInterface {

        @GET("android/jsonarray/")
        Observable<List<AndroidVersion>> register();
    }
}
