package com.example.km.genericapp.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.km.genericapp.R;
import com.example.km.genericapp.activities.PostDetailsActivity;
import com.example.km.genericapp.adapters.PostListCardAdapter;
import com.example.km.genericapp.interfaces.RecyclerViewClickListener;
import com.example.km.genericapp.models.posts.BlogItem;
import com.example.km.genericapp.network.PostalApiService;
import com.example.km.genericapp.utilities.SnackbarHelper;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Displays a recycler view where each card shows just the post title and avatar image.
 */
public class PostsListFragment extends BaseRecyclerFragment implements RecyclerViewClickListener {

    private CompositeDisposable disposables;
    private PostListCardAdapter adapter;

    @Override
    public void onClick(View view, int position, BlogItem blogItem) {
        PostDetailsActivity.start(getActivity(), blogItem.getPost());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        View view = inflater.inflate(R.layout.fragment_post_list, container, false);
        setupScreenLayout(view);
        disposables = new CompositeDisposable();
        initializeRecyclerView();
        loadApiData();
        return view;
    }

    private void loadApiData() {
        showProgressIndicator();
        disposables.add(PostalApiService.getBlogItems()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<ArrayList<BlogItem>>() {
                    @Override
                    public void onComplete() {
                        hideProgressIndicator();
                        SnackbarHelper.showSnackbar(getActivity(), recyclerView, getActivity().getString(R.string.api_call_complete));
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        handleError(throwable);
                    }

                    @Override
                    public void onNext(ArrayList<BlogItem> blogItems) {
                        handleResponse(blogItems);
                    }
                }));
    }

    private void handleResponse(ArrayList<BlogItem> blogItems) {
        blogItems = filterlLogItems(blogItems);
        adapter = new PostListCardAdapter(blogItems, this);
        recyclerView.setAdapter(adapter);
    }

    /**
     * Filter the blog items so that only post id 1 is selected, purely as a test of filtering.
     */
    public ArrayList<BlogItem> filterlLogItems(ArrayList<BlogItem> blogItems) {
        List<BlogItem> results = blogItems.stream()
                .filter(blogItem -> (blogItem.getPost().getId() == 1) || (blogItem.getPost().getId() == 2))
                .collect(Collectors.toList());
        results.forEach(System.out::println);
        return new ArrayList<BlogItem>(results);
    }

    private void handleError(Throwable error) {
        SnackbarHelper.showSnackbar(getActivity(), recyclerView, getActivity().getString(R.string.api_call_error) + error.getLocalizedMessage());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        disposables.clear();
    }
}
