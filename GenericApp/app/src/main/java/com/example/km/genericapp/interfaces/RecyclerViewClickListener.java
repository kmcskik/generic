package com.example.km.genericapp.interfaces;

import android.view.View;

import com.example.km.genericapp.models.posts.BlogItem;

public interface RecyclerViewClickListener {

    void onClick(View view, int position, BlogItem blogItem);
}
