package com.example.km.genericapp.fragments;

import android.support.v4.app.Fragment;
import android.view.View;

import com.example.km.genericapp.R;

public class BaseFragment extends Fragment {

    private View progressOverlay;

    public void showProgressIndicator() {
        progressOverlay.setVisibility(View.VISIBLE);
    }

    public void hideProgressIndicator() {
        progressOverlay.setVisibility(View.GONE);
    }

    protected void setupScreenLayout(View view) {
        progressOverlay = view.findViewById(R.id.progressOverlay);
    }
}
