package com.example.km.genericapp.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.km.genericapp.R;
import com.example.km.genericapp.interfaces.RecyclerViewClickListener;
import com.example.km.genericapp.models.posts.BlogItem;
import com.example.km.genericapp.utilities.ImageHelper;

/**
 * Post list card view holder.
 */
public class PostListCardViewHolder extends RecyclerView.ViewHolder {

    private RecyclerViewClickListener listener;
    protected int position;
    protected BlogItem blogItem;
    protected LinearLayout selectLayoutView;
    private final ImageView avatarImageView;
    private final TextView titleTextView;

    public PostListCardViewHolder(View view, final RecyclerViewClickListener listener) {
        super(view);
        this.listener = listener;
        selectLayoutView = (LinearLayout) view.findViewById(R.id.selectLayout);
        avatarImageView = (ImageView) itemView.findViewById(R.id.avatar);
        titleTextView = (TextView) itemView.findViewById(R.id.title);
        selectLayoutView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onClick(view, position, blogItem);
            }
        });
    }

    public void setData(final BlogItem blogItem, final int position) {
        this.position = position;
        this.blogItem = blogItem;
        titleTextView.setText(blogItem.getPost().getTitle());
        ImageHelper.loadAvatar(avatarImageView, blogItem.getUser().getEmail());
    }
}
