package com.example.km.genericapp.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.km.genericapp.R;
import com.example.km.genericapp.adapters.PostListWithDetailsCardAdapter;
import com.example.km.genericapp.models.posts.BlogItem;
import com.example.km.genericapp.network.PostalApiService;
import com.example.km.genericapp.utilities.SnackbarHelper;

import java.util.ArrayList;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Displays a recycler view where each card shows the full post details
 * including the title, body, username, and number of comments.
 */
public class PostsListWithDetailsFragment extends BaseRecyclerFragment {

    private CompositeDisposable disposables;
    private PostListWithDetailsCardAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        View view = inflater.inflate(R.layout.fragment_post_list_with_details, container, false);
        setupScreenLayout(view);
        disposables = new CompositeDisposable();
        initializeRecyclerView();
        loadApiData();
        return view;
    }

    private void loadApiData() {
        showProgressIndicator();
        disposables.add(PostalApiService.getBlogItems()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<ArrayList<BlogItem>>() {
                    @Override
                    public void onComplete() {
                        hideProgressIndicator();
                        SnackbarHelper.showSnackbar(getActivity(), recyclerView, getActivity().getString(R.string.api_call_complete));
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        handleError(throwable);
                    }

                    @Override
                    public void onNext(ArrayList<BlogItem> blogItems) {
                        handleResponse(blogItems);
                    }
                }));
    }

    private void handleResponse(ArrayList<BlogItem> blogItems) {
        adapter = new PostListWithDetailsCardAdapter(blogItems);
        recyclerView.setAdapter(adapter);
    }

    private void handleError(Throwable error) {
        SnackbarHelper.showSnackbar(getActivity(), recyclerView, getActivity().getString(R.string.api_call_error) + error.getLocalizedMessage());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        disposables.clear();
    }
}
