package com.example.km.genericapp.fragments;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.example.km.genericapp.R;

public class BaseRecyclerFragment extends BaseFragment {

    protected RecyclerView recyclerView;

    protected void initializeRecyclerView() {
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
    }

    @Override
    protected void setupScreenLayout(View view) {
        super.setupScreenLayout(view);
        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
    }
}
