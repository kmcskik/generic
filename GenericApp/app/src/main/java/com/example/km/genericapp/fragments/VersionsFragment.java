package com.example.km.genericapp.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.km.genericapp.R;
import com.example.km.genericapp.adapters.AndroidVersionAdapter;
import com.example.km.genericapp.models.versions.AndroidVersion;
import com.example.km.genericapp.network.AndroidVersionApiService;
import com.example.km.genericapp.utilities.SnackbarHelper;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

public class VersionsFragment extends BaseRecyclerFragment {

    private CompositeDisposable disposables;
    private AndroidVersionAdapter adapter;
    private ArrayList<AndroidVersion> androidArrayList;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        View view = inflater.inflate(R.layout.fragment_versions, container, false);
        setupScreenLayout(view);
        disposables = new CompositeDisposable();
        initializeRecyclerView();
        loadApiData();
        return view;
    }

    /**
     * Load all Android versions.
     */
    private void loadApiData() {
        showProgressIndicator();
        disposables.add(AndroidVersionApiService.getAndroidVersions()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<List<AndroidVersion>>() {
                    @Override
                    public void onComplete() {
                        hideProgressIndicator();
                        SnackbarHelper.showSnackbar(getActivity(), recyclerView, getActivity().getString(R.string.api_call_complete));
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        handleError(throwable);
                    }

                    @Override
                    public void onNext(List<AndroidVersion> androidList) {
                        handleResponse(androidList);
                    }
                }));
    }

    private void handleResponse(List<AndroidVersion> androidList) {
        androidArrayList = new ArrayList<>(androidList);
        adapter = new AndroidVersionAdapter(androidArrayList);
        recyclerView.setAdapter(adapter);
    }

    private void handleError(Throwable error) {
        SnackbarHelper.showSnackbar(getActivity(), recyclerView, getActivity().getString(R.string.api_call_error) + error.getLocalizedMessage());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        disposables.clear();
    }
}
