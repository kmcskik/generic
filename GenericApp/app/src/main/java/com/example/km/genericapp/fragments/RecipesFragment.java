package com.example.km.genericapp.fragments;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;

import com.example.km.genericapp.R;
import com.example.km.genericapp.adapters.RecipeAdapter;
import com.example.km.genericapp.constants.Constants;
import com.example.km.genericapp.models.recipes.Recipes;
import com.example.km.genericapp.network.RecipeApiService;
import com.example.km.genericapp.utilities.SnackbarHelper;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

public class RecipesFragment extends BaseRecyclerFragment {

    private CompositeDisposable disposables;
    private RecipeAdapter adapter;
    protected EditText searchText;
    private ProgressBar searchProgressBar;
    private ImageButton searchClearButton;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        View view = inflater.inflate(R.layout.fragment_recipes, container, false);
        setupScreenLayout(view);
        disposables = new CompositeDisposable();
        initializeRecyclerView();
        loadApiData(Constants.EMPTY_STRING);
        return view;
    }

    private void loadApiData(String query) {
        showProgressIndicator();
        disposables.add(RecipeApiService.getRecipes(query)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<Recipes>() {
                    @Override
                    public void onComplete() {
                        hideProgressIndicator();
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        handleError(throwable);
                    }

                    @Override
                    public void onNext(Recipes recipes) {
                        handleResponse(recipes);
                    }
                }));
    }

    private void handleResponse(Recipes recipes) {
        adapter = new RecipeAdapter(recipes);
        recyclerView.setAdapter(adapter);
    }

    private void handleError(Throwable error) {
        SnackbarHelper.showSnackbar(getActivity(), recyclerView, getActivity().getString(R.string.api_call_error) + error.getLocalizedMessage());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        disposables.clear();
    }

    @Override
    protected void setupScreenLayout(View view) {
        super.setupScreenLayout(view);
        searchProgressBar = (ProgressBar) view.findViewById(R.id.searchProgressBar);
        searchClearButton = (ImageButton) view.findViewById(R.id.searchClearButton);
        searchText = (EditText) view.findViewById(R.id.searchEditText);
        searchText.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                handleSearchTextUpdate();
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        searchClearButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                searchText.setText(Constants.EMPTY_STRING);
            }
        });

        //noinspection deprecation
        searchProgressBar.getIndeterminateDrawable().setColorFilter(
                getResources().getColor(R.color.colorAccent),
                android.graphics.PorterDuff.Mode.MULTIPLY);
    }

    private void handleSearchTextUpdate() {
        final String query = searchText.getText().toString();
        if (TextUtils.isEmpty(query)) {
            searchClearButton.setVisibility(View.GONE);
        } else {
            searchClearButton.setVisibility(View.VISIBLE);
        }
        loadApiData(query);
    }
}
